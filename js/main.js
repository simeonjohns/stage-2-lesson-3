function Calculator(elemId) {

	var calcElem = document.getElementById(elemId),
		display =  calcElem.querySelector('#calc-display'),
		curNum = [];
		args = {
			x1: 		'',
			x2: 		'',
			method: 	'',
			operation: 	''
		};

	(this.clear = function() {								// ФУНКЦИЯ ОЧИСТКИ ПОЛЯ И ВРЕМЕННЫХ ДАННЫХ
		display.innerHTML = '';
		for (var arg in args) {
			args[arg] = '';
		}
		curNum = [];
	}).call(this); 											// (call, модуль) Вызываем сразу же через модуль и привязку контекста - для первоначальной очистки поля


	function sum() { return this.x1 + this.x2; };
	function sub() { return this.x1 - this.x2; };
	function mult() { return this.x1 * this.x2; };

	function getResult() { 									// ФУНКЦИЯ ПОЛУЧЕНИЯ РЕЗУЛЬТАТА ВЫЧЕСЛЕНИЯ
		if (display.innerHTML == '') return; 				// Проверяем поле на отсутствие значений - избегаем undefined

		curNum = [this.method()];
		display.innerHTML = curNum;

		args.operation = '';
	}

	function switchNumbers(x) { 							// ФУНКЦИЯ СМЕНЫ ТЕКУЩЕГО ЧИСЛА
		args[x] = parseInt(curNum.join(''));				// (join)
		curNum = [];
	}

	function checkBinarInputs() {							// ФУНКЦИЯ ПРОВЕРКИ ВВЕДЕННЫХ ДАННЫХ НА 2 АРГУМЕНТА
		return {
			method: (args.x1 == 0 && curNum == 0) || args.operation != 0,
			equal: 	args.operation == 0 || curNum == 0
		};
	}

	this.output = getResult.bind(args);						// (bind) Привязываем контекст args к функции получения результата

	calcElem.addEventListener('click', function(e) { 		// ЛИСЕНЕР НА КЛИК ПО КНОПКЕ
		var target = e.target;

		if (!target.classList.contains('button')) return; 	// Проверяем нажатие на кнопку

		switch (target.getAttribute('value')) {
			case '=':
				if (checkBinarInputs().equal) return;
				switchNumbers('x2');
				return this.output(); 						// Если нажата кнопка "Равно" - получаем результат
				break;

			case 'C':
				return this.clear(); 						// Если нажата кнопка "С" - очищаем поле
				break;

			case '+':
				if (checkBinarInputs().method) return;
				args.method = sum;
				switchNumbers('x1');
				break;

			case '-':
				if (checkBinarInputs().method) return;
				args.method = sub;
				switchNumbers('x1');
				break;

			case '*':
				if (checkBinarInputs().method) return;
				args.method = mult;
				switchNumbers('x1');
				break;

		}

		if (!isNaN(target.getAttribute('value'))) {
			curNum.push(target.getAttribute('value'));
		} else {
			args.operation = target.getAttribute('value');
		}

		console.log(args);
		console.log(curNum);

		display.innerHTML += target.getAttribute('value');	// Вписываем символ в поле
	}.bind(this)); 											// (bind) Привязываем контекст класса к функции

}

var calc = new Calculator('calc');
